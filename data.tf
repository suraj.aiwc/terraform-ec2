data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "var.bucket"
    key    = "newdev2/ec2/learning1/terraform.tfstate"
    region = "var.region"

    }
  }


